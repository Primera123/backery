package com.example.cake;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartcakefactoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartcakefactoryApplication.class, args);
	}

}
